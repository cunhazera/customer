# Customer CRUD

# Running the project

## Required software

You need to have a Java 11 and Maven 3.6.x installed

## Building

To run this project on your machine you only need to execute these two steps:

 - `mvn clean install`
 - `docker-compose up --build`
 
## Testing

To test this application you have the postman json file: `Customer.postman_collection.json`. Import it to your Postman and execute the requests. Have fun!