package com.cunha.customer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomerUpdateDTO {

    private Long id;
    private String name;
    private Long cpf;
    private LocalDate dateOfBirth;

}
