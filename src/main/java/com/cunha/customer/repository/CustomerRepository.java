package com.cunha.customer.repository;

import com.cunha.customer.entity.Customer;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {

    List<Customer> findAll(Specification<Customer> spec, Pageable pageable);

}
