package com.cunha.customer.service;

import com.cunha.customer.dto.CustomerDTO;
import com.cunha.customer.dto.CustomerUpdateDTO;
import com.cunha.customer.entity.Customer;
import com.cunha.customer.exception.CustomerNotFoundException;
import com.cunha.customer.repository.CustomerRepository;
import com.cunha.customer.vo.CustomerVO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository repository;

    public List<CustomerVO> findAll(int page, int size, Long cpf, String name) {
        List<Customer> customers;
        if (cpf == null && name == null) {
            customers = repository.findAll(PageRequest.of(page, size)).toList();
        } else {
            customers = repository.findAll(Specification.where(CustomerSpecs.numberInCpf(cpf)).or(CustomerSpecs.textInName(name)), PageRequest.of(page, size));
        }
        return customers
                .stream()
                .map(e -> new CustomerVO(
                        e.getId(),
                        e.getName(),
                        e.getCpf(),
                        e.getDateOfBirth(),
                        Period.between(e.getDateOfBirth(), LocalDate.now()).getYears()
                )).collect(Collectors.toList());
    }

    @Transactional
    public CustomerVO create(CustomerDTO customerDTO) {
        Customer customer = repository.save(new Customer(customerDTO.getName(), customerDTO.getCpf(), customerDTO.getDateOfBirth()));
        return new CustomerVO(
                customer.getId(),
                customer.getName(),
                customer.getCpf(),
                customer.getDateOfBirth(),
                Period.between(customer.getDateOfBirth(), LocalDate.now()).getYears()
        );
    }

    @Transactional
    public void delete(long id) {
        repository.findById(id).ifPresentOrElse(e -> repository.deleteById(id),
                () -> {
                    throw new CustomerNotFoundException(id);
                });
    }

    @Transactional
    public CustomerUpdateDTO updateCustomer(CustomerUpdateDTO customerDTO) {
        Customer customer = repository.findById(customerDTO.getId()).orElseThrow(() -> new CustomerNotFoundException(customerDTO.getId()));
        customer.setCpf(customerDTO.getCpf());
        customer.setDateOfBirth(customerDTO.getDateOfBirth());
        customer.setName(customerDTO.getName());
        repository.save(customer);
        return new CustomerUpdateDTO(
                customer.getId(),
                customer.getName(),
                customer.getCpf(),
                customer.getDateOfBirth()
        );
    }

    @Transactional
    public void patchCustomer(long id, JsonPatch patch) throws JsonPatchException, JsonProcessingException {
        Customer customer = repository.findById(id).orElseThrow(() -> new CustomerNotFoundException(id));
        repository.save(applyPatchToCustomer(patch, customer));
    }

    private Customer applyPatchToCustomer(
            JsonPatch patch, Customer targetCustomer) throws JsonPatchException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        JsonNode patched = patch.apply(objectMapper.convertValue(targetCustomer, JsonNode.class));
        return objectMapper.treeToValue(patched, Customer.class);
    }

}
