package com.cunha.customer.service;

import com.cunha.customer.entity.Customer;
import org.springframework.data.jpa.domain.Specification;

public class CustomerSpecs {

    public static Specification<Customer> textInName(String name) {
        return (root, query, builder) ->
                        builder.equal(root.get("name"), name);
    }

    public static Specification<Customer> numberInCpf(Long cpf) {
        return (root, query, builder) ->
                        builder.equal(root.get("cpf"), cpf);
    }

}
