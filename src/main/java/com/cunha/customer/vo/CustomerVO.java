package com.cunha.customer.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerVO {

    private Long id;
    private String name;
    private Long cpf;
    private LocalDate dateOfBirth;
    private Integer age;

}
