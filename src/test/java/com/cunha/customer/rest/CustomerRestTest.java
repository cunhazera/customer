package com.cunha.customer.rest;

import com.cunha.customer.CustomerApplication;
import com.cunha.customer.dto.CustomerDTO;
import com.cunha.customer.dto.CustomerUpdateDTO;
import com.cunha.customer.entity.Customer;
import com.cunha.customer.repository.CustomerRepository;
import com.cunha.customer.vo.CustomerVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = CustomerApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class CustomerRestTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository repository;

    private ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

    private static final String ERROR_MESSAGE = "The customer with id %d was not found.";

    @Test
    public void testCreateNewCustomer() throws Exception {
        CustomerDTO customer = new CustomerDTO("Name", 10589874637L, LocalDate.now());
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/customers")
                .content(mapper.writeValueAsBytes(customer))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);
        MockHttpServletResponse response = mockMvc.perform(requestBuilder).andReturn().getResponse();
        assertThat(response.getStatus(), equalTo(200));
        CustomerVO customerVO = mapper.readValue(response.getContentAsString(), CustomerVO.class);
        assertThat(customerVO.getAge(), equalTo(0));
        assertThat(customerVO.getCpf(), equalTo(10589874637L));
        assertThat(customerVO.getDateOfBirth(), equalTo(LocalDate.now()));
        assertThat(customerVO.getId(), greaterThan(0L));
    }

    @Test
    public void testFindCustomers() throws Exception {
        repository.saveAll(Arrays.asList(
                new Customer("Name1", 12312312312L, LocalDate.of(2018, 5, 20)),
                new Customer("Name2", 11901901912L, LocalDate.of(1990, 4, 13)),
                new Customer("Name3", 98748785987L, LocalDate.of(1997, 2, 12)),
                new Customer("Name4", 12023698745L, LocalDate.of(2002, 10, 25))
        ));
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/customers")
                .queryParam("name", "Name1")
                .queryParam("cpf", "12023698745")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);
        MockHttpServletResponse response = mockMvc.perform(requestBuilder).andReturn().getResponse();
        assertThat(response.getStatus(), equalTo(200));
        List<CustomerVO> customerResult = mapper.readValue(response.getContentAsString(), mapper.getTypeFactory().constructCollectionType(List.class, CustomerVO.class));
        assertThat(customerResult, hasSize(2));
    }

    @Test
    public void testUpdateCustomer() throws Exception {
        Customer persisted = repository.save(new Customer("Name1", 12312312312L, LocalDate.of(2018, 5, 20)));
        CustomerUpdateDTO customer = new CustomerUpdateDTO(persisted.getId(), "NewName", 32132132112L, LocalDate.now());
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(String.format("/customers", persisted.getId()))
                .content(mapper.writeValueAsBytes(customer))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);
        MockHttpServletResponse response = mockMvc.perform(requestBuilder).andReturn().getResponse();
        CustomerVO newCustomer = mapper.readValue(response.getContentAsString(), CustomerVO.class);
        assertThat(newCustomer.getName(), equalTo("NewName"));
        assertThat(newCustomer.getCpf(), equalTo(32132132112L));
    }

    @Test
    public void testUpdateCustomerNotFound() throws Exception {
        CustomerUpdateDTO customer = new CustomerUpdateDTO(11L, "NewName", 32132132112L, LocalDate.now());
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(String.format("/customers", 11L))
                .content(mapper.writeValueAsBytes(customer))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);
        MockHttpServletResponse response = mockMvc.perform(requestBuilder).andReturn().getResponse();
        assertThat(response.getStatus(), equalTo(404));
        assertThat(mapper.readTree(response.getContentAsString()).get("message").asText(), equalTo(String.format(ERROR_MESSAGE, 11L)));
    }

    @Test
    public void testDeleteCustomer() throws Exception {
        Customer persisted = repository.save(new Customer("Name1", 12312312312L, LocalDate.of(2018, 5, 20)));
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(String.format("/customers/%d", persisted.getId()));
        MockHttpServletResponse response = mockMvc.perform(requestBuilder).andReturn().getResponse();
        assertThat(response.getStatus(), equalTo(204));
        assertThat(repository.existsById(persisted.getId()), is(false));
    }

    @Test
    public void testDeleteCustomerNotFound() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(String.format("/customers/%d", 123123L));
        MockHttpServletResponse response = mockMvc.perform(requestBuilder).andReturn().getResponse();
        assertThat(response.getStatus(), equalTo(404));
        assertThat(mapper.readTree(response.getContentAsString()).get("message").asText(), equalTo(String.format(ERROR_MESSAGE, 123123L)));
    }

    @Test
    public void testPatchCustomerNotFound() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .patch(String.format("/customers/%d", 123123L))
                .contentType("application/json-patch+json")
                .content("[{\"op\":\"replace\",\"path\":\"/name\",\"value\":\"Oi\"}]");
        MockHttpServletResponse response = mockMvc.perform(requestBuilder).andReturn().getResponse();
        assertThat(response.getStatus(), equalTo(404));
        assertThat(mapper.readTree(response.getContentAsString()).get("message").asText(), equalTo(String.format(ERROR_MESSAGE, 123123L)));
    }

    @Test
    public void testPatchCustomer() throws Exception {
        Customer persisted = repository.save(new Customer("Name1", 123123L, LocalDate.of(2018, 5, 20)));
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .patch(String.format("/customers/%d", persisted.getId()))
                .contentType("application/json-patch+json")
                .content("[{\"op\":\"replace\",\"path\":\"/name\",\"value\":\"NewNameFromPatch\"}]");
        MockHttpServletResponse response = mockMvc.perform(requestBuilder).andReturn().getResponse();
        Customer newCustomer = repository.findById(persisted.getId()).get();
        assertThat(response.getStatus(), equalTo(204));
        assertThat(newCustomer.getName(), equalTo("NewNameFromPatch"));
        assertThat(newCustomer.getCpf(), equalTo(persisted.getCpf()));
        assertThat(newCustomer.getDateOfBirth(), equalTo(persisted.getDateOfBirth()));
    }

}
