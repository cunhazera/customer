package com.cunha.customer.service;

import com.cunha.customer.dto.CustomerDTO;
import com.cunha.customer.dto.CustomerUpdateDTO;
import com.cunha.customer.entity.Customer;
import com.cunha.customer.exception.CustomerNotFoundException;
import com.cunha.customer.repository.CustomerRepository;
import com.cunha.customer.vo.CustomerVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomerServiceTest {

    @Mock
    private CustomerRepository repository;

    @InjectMocks
    private CustomerService service;

    @BeforeAll
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindCustomers() {
        Mockito.when(repository.findAll(Mockito.any(), Mockito.any())).thenReturn(Arrays.asList(new Customer("Name", 105l, LocalDate.now())));
        List<CustomerVO> customers = service.findAll(0, 10, 105l, "Name");
        assertThat(customers, hasSize(1));
    }

    @Test
    public void testDeleteCustomerNotFound() {
        Mockito.when(repository.findById(123l)).thenReturn(Optional.empty());
        Assertions.assertThrows(CustomerNotFoundException.class, () -> service.delete(123l));
    }

    @Test
    public void testUpdateCustomerNotFound() {
        Mockito.when(repository.findById(1l)).thenReturn(Optional.empty());
        Assertions.assertThrows(CustomerNotFoundException.class, () -> {
            CustomerUpdateDTO customerDTO = new CustomerUpdateDTO();
            customerDTO.setId(1l);
            service.updateCustomer(customerDTO);
        });
    }

    @Test
    public void testPatchCustomerNotFound() {
        Mockito.when(repository.findById(1l)).thenReturn(Optional.empty());
        Assertions.assertThrows(CustomerNotFoundException.class, () -> {
            CustomerDTO customerDTO = new CustomerDTO();
            service.patchCustomer(1l, Mockito.any());
        });
    }

}
